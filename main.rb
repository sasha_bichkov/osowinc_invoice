require_relative 'generators/invoice/pdf'

ORDER_ID = '#em9yrcpqsasd'
FROM = 'Manuelito, NM 87319, USA'
TO = 'Commerce City, CO, USA'
ITEMS = []

(0..rand(100)).each.with_index do |item, index|
  item = {
    title: "Item ##{index}",
    description: "Description #{index}",
    amount: rand(0.0..10000.9).round(2)
  }

  ITEMS.push(item)
end

invoice = Generators::Invoice::Pdf.new(ORDER_ID, FROM, TO, ITEMS).call
