require 'prawn'
require 'prawn/table'

module Generators
  module Invoice
    class Pdf

      def initialize(order_id, from, to, items)
        @order_id = order_id
        @from = from
        @to = to
        @items = items
        @total = @items.map { |item| item[:amount] }.sum.round(2)
      end

      def call
        create_invoice('./invoice.pdf') do |invoice|
          add_settings(invoice)
          render_header(invoice)
          render_items(invoice)
        end
      end

      private

      PAGE_SIZE = 'A4'
      HEADER_HEIGHT = 50
      HEADER_FONT_SIZE_TITLE = 30
      HEADER_MIDDLE_FONT_SIZE = 18
      HEADER_SMALL_FONT_SIZE = 12
      TOTAL_FONT_SIZE = 18

      TABLE_PADDING = 10      
      MAX_ELEMENTS_FOR_THE_FIRST_PAGE = 15
      MAX_ELEMENTS_FOR_OTHER_PAGES = 20

      PURPLE_COLOR = '6059f6'
      BLACK_COLOR = '000000'
      WHITE_COLOR = 'FFFFFF'
      GREY_COLOR = 'F8F8FF'
      LINK_COLOR = '05FDFF'

      CELLS_PADDING = 12

      AMOUNT_COLUMN_INDEX = 2

      def create_invoice(path)
        invoice = Prawn::Document.new(page_size: PAGE_SIZE, margin: 0)
        yield invoice
        invoice.render_file(path)
      end

      def add_settings(invoice)
        @bounds = invoice.bounds
        load_fonts(invoice)
      end

      def render_header(invoice)
        invoice.bounding_box([0, @bounds.height], width: @bounds.right, height: mm2pt(HEADER_HEIGHT)) do
          render_header_background(invoice)
          render_header_title(invoice)
          render_header_invoice_info(invoice)
        end
      end

      def render_header_background(invoice)
        invoice.fill_color(PURPLE_COLOR)
        invoice.fill_rectangle([0, mm2pt(HEADER_HEIGHT)], @bounds.right, mm2pt(HEADER_HEIGHT))
        invoice.fill_color(BLACK_COLOR)
      end

      def render_header_title(invoice)
        header_vertical_middle = mm2pt(HEADER_HEIGHT + 25) / 2

        invoice.bounding_box([mm2pt(10), header_vertical_middle], width: 180, height: 90) do
          invoice.fill_color(WHITE_COLOR)
          invoice.font('OpenSans', style: :bold)
          invoice.text_box('OSOW INC', size: HEADER_FONT_SIZE_TITLE, at: [0, 90])
          invoice.font('OpenSans', style: :regular)
          invoice.text_box('Call: (123) 123456', size: HEADER_SMALL_FONT_SIZE, at: [0, 55])
          invoice.text_box('Email: info@osowinc.com', size: HEADER_SMALL_FONT_SIZE, at: [0, 35])
          invoice.text_box('Site: https://osowinc.com', size: HEADER_SMALL_FONT_SIZE, at: [0, 15])
        end
      end

      def render_header_invoice_info(invoice)
        header_vertical_middle = mm2pt(HEADER_HEIGHT + 25) / 2

        invoice.bounding_box([mm2pt(77), header_vertical_middle], width: 350, height: 80) do
          invoice.fill_color(WHITE_COLOR)
          invoice.font('OpenSans', style: :bold)
          invoice.text_box('Invoice', size: HEADER_MIDDLE_FONT_SIZE, align: :right, at: [0, 80])
          invoice.font('OpenSans', style: :regular)
          invoice.text_box("Permit order #{link(@order_id, 'https://osowinc.com')}", inline_format: true, size: HEADER_SMALL_FONT_SIZE, align: :right, at: [0, 55])
          invoice.text_box("From: #{@from}", size: HEADER_SMALL_FONT_SIZE, align: :right, at: [0, 36])
          invoice.text_box("To: #{@to}", size: HEADER_SMALL_FONT_SIZE, align: :right, at: [0, 16])
        end
      end

      def render_items(invoice)
        rows = prepared_rows
        pages = rows.each_slice(MAX_ELEMENTS_FOR_OTHER_PAGES)
        pages_count = pages.size

        render_initial_table(invoice, rows, pages_count)
        render_pages(invoice, pages) unless pages_count.zero?
      end

      def render_initial_table(invoice, rows, pages_count)
        initial_rows = rows.slice!(0..MAX_ELEMENTS_FOR_THE_FIRST_PAGE)
        position = @bounds.height - mm2pt(HEADER_HEIGHT + TABLE_PADDING)
        render_table(invoice, 0, initial_rows, position, pages_count == 1)
      end

      def render_pages(invoice, pages)
        pages.each_with_index do |current_rows, index|
          invoice.start_new_page
          current_index = index + 1
          is_last = current_index == pages.size
          pos_y = @bounds.height - mm2pt(TABLE_PADDING)
          render_table(invoice, current_index, current_rows, pos_y, is_last)
        end
      end

      def render_table(invoice, table_index, rows, pos_y, is_last)
        width = @bounds.right - mm2pt(TABLE_PADDING * 2)
        columns_width = [mm2pt(50), mm2pt(112), mm2pt(28)]

        add_styles_table_header = -> (row) { add_styles_table_header(row) }
        add_styles_table_total = -> (row) { add_styles_table_total(row) }

        invoice.bounding_box([mm2pt(TABLE_PADDING), pos_y], width: width, height: @bounds.height) do
          invoice.fill_color(BLACK_COLOR)
          invoice.font('OpenSans', style: :regular)
          invoice.table(rows, position: :center, column_widths: columns_width, row_colors: [GREY_COLOR, WHITE_COLOR]) do
            cells.padding = CELLS_PADDING
            cells.color = PURPLE_COLOR
            cells.borders = [:bottom]
            columns([AMOUNT_COLUMN_INDEX]).align = :right

            add_styles_table_header[row(0)] if table_index.zero?
            add_styles_table_total[row(-1)] if is_last
          end
        end
      end

      def add_styles_table_header(row)
        row.text_color = PURPLE_COLOR
        row.borders = [:bottom]
        row.border_width = 2
        row.font_style = :bold
      end

      def add_styles_table_total(row)
        row.align = :right
        row.border_width = 0
        row.background_color = WHITE_COLOR
        row.text_color = PURPLE_COLOR
        row.size = TOTAL_FONT_SIZE
      end

      def prepared_rows
        rows = [['Title', 'Description', 'Amount']]
        data = @items.map(&:values)
        total_row = [[{ content: "Total: $#{@total}", colspan: 3 }]]
        rows.concat(data, total_row)
      end

      def link(title, url)
        "<color rgb='##{LINK_COLOR}'><link href='#{url}'>#{title}</link></color>"
      end

      def load_fonts(invoice)
        invoice.font_families.update(
          'OpenSans' => {
            regular: path_to_font('opensans/OpenSans-Regular.ttf'),
            bold: path_to_font('opensans/OpenSans-Bold.ttf')
          }
        )
      end

      def path_to_font(path)
        File.expand_path("fonts/#{path}", File.dirname(__FILE__))
      end

      def mm2pt(mm)
        mm * (72 / 25.4)
      end

    end
  end
end
